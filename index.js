//load express after you have done npm install --save express
var express = require("express");
//Create an application
var app = express ();

//Use this directory as the document root

app.use(express.static(__dirname + "/public")  );

//Start our web server on port 3000
app.listen(3000, function(){
    console.info("My Web Server has started on port 3000");
    console.info("document root is at " +__dirname + "/public");
    
});
